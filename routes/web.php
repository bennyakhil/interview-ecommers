<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['as'=>'home','uses'=>'HomeController@index']);
Route::get('category/{id}',['as'=>'category','uses'=>'HomeController@productList']);
Route::get('product-details/{id}',['as'=>'product.details','uses'=>'HomeController@productDetails']);

Route::group(['as'=>'admin.'],function(){
Route::get('category',['as'=>'category','uses'=>'CategoryController@index']);
Route::get('product',['as'=>'product','uses'=>'ProductController@index']);
});
