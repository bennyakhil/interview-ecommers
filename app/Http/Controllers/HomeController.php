<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;

class HomeController
{
    public function index()
    {
        $categories = Category::all();
        return view('main',compact('categories'));
    }
    public function productList($slug)
    {

        $category = Category::where('category_name_slug',$slug)->first();
        $products = Product::where('category_id',$category->category_id)->get();
        return view('product-list',compact('products'));
    }
    public function productDetails($slug)
    {
        $product = Product::where('product_name_slug',$slug)->first();
        return view('product-details',compact('product'));
    }
}
